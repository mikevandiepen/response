# Response Messages
Simple response messages for custom tools.

### Installation
###### Composer
```console
$ composer require mikevandiepen/response
```
###### GIT
```console
$ git clone https://mikevandiepen@bitbucket.org/mikevandiepen/response.git
```

### Requirements
```json
{
    "require": {
        "PHP": "^7.0"
    }
}
```



### Usage
###### Basic message
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages and returning a json response
$response->add('Action executed successfully')->toJSON();
?>
```
###### Response
```json
[
	{"level":"primary","message":"Action executed successfully"}
]
```



###### Single message
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('Action executed successfully', [ /** There must be an array, but can be left empty*/ ], Response::SUCCESS);

// Returning the message in json response
$response->toJSON();
?>
```
###### Response
```json
[
	{"level":"success","message":"Action executed successfully"}
]
```



###### Multiple parameters
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('{%action%} executed {%status%}', ['action' => 'My Custom Action', 'status' => 'successfully'], Response::SUCCESS);

// Returning the message in json response
$response->toJSON();
?>
```
###### Response
```json
[
	{"level":"success","message":"My Custom Action executed successfully"}
]
```



###### Multiple messages with parameters
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('{%action%} executed successfully', ['action' => 'your_action'], Response::SUCCESS);
$response->add('DUPLICATE {%action%}!', ['action' => 'your_action_2'], Response::WARNING);
$response->add('DUPLICATE {%action%}!', ['action' => 'your_action_3'], Response::WARNING);
$response->add('{%action%} does not exist', ['action' => 'your_action_4'], Response::ERROR);

// Returning the messages in json response
$response->toJSON();
?>
```
###### Response
```json
[
	{"level":"success","message":"your_action executed successfully"},
	{"level":"warning","message":"DUPLICATE your_action_2!"},
	{"level":"warning","message":"DUPLICATE your_action_3!"},
	{"level":"danger","message":"your_action_4 does not exist"}
]
```

###### Highlight parameters
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('Look how cool this is! {%parameter%}', ['parameter' => 'Highlight me!'], Response::SUCCESS)->delimiters('"');

// Returning the message in json response
$response->toJSON();
?>
```
###### Response
```json
[
	{"level":"success","message":"Look how cool this is! \"Highlight me!\""}
]
```

In this case we used only one encapsulation string which were applied to both sides.
If in your case you want different characters on either side you can add a second item to the array.

For example:
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('Look how cool this is! {%parameter%}', ['parameter' => 'Highlight me!'], Response::SUCCESS)->delimiters('<strong>', '</strong>');

// Returning the message in json response
$response->toJSON();
?>
```
###### Response
```json
[
	{"level":"success","message":"Look how cool this is! <strong>Highlight me!</strong>"}
]
```

###### Return responses as an array
```php
<?php
use mikevandiepen\utility\Response;

// Instantiating the Response class
$response = new Response();

// Adding messages
$response->add('Look how cool this is! {%parameter%}', ['parameter' => 'Highlight me!'], Response::SUCCESS)->delimiters('"');

// Returning the message in json response
$response->toArray();
?>
```
###### Response
```php
array(
    "level"     => "success",
    "message"   => "Look how cool this is! \"Highlight me!\""
);
```


### licence
###### MIT
Copyright 2019 mikevandiepen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.